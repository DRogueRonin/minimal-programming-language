<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class LetStatement
{
  public ?Token $token = null;
  public ?Identifier $name = null;
  public mixed $value = null;

  public static function parse(Parser $parser): ?LetStatement
  {
    $statement = new LetStatement();

    $statement->token = $parser->curToken;

    $peek = $parser->expectPeek(TokenType::IDENTIFIER);
    if (! $peek) {
      return null;
    }

    $statement->name = new Identifier($parser->curToken, $parser->curToken->literal);

    $peek = $parser->expectPeek(TokenType::ASSIGN);
    if (! $peek) {
      return null;
    }

    $parser->nextToken();

    $statement->value = $parser->parseExpression(Parser::LOWEST);

    if ($parser->peekTokenIs(TokenType::SEMICOLON)) {
      $parser->nextToken();
    }

    return $statement;
  }
}

