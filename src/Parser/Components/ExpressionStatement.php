<?php

namespace Mnml\Parser\Components;

use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;
use Mnml\Parser\Parser;

class ExpressionStatement
{
  public ?Token $token = null;
  public mixed $expression = null;

  public static function parse(Parser $parser): ExpressionStatement
  {
    $statement = new ExpressionStatement();

    $statement->token = $parser->curToken;

    $statement->expression = $parser->parseExpression(Parser::LOWEST);

    if ($parser->peekTokenIs(TokenType::SEMICOLON)) {
      $parser->nextToken();
    }

    return $statement;
  }
}

