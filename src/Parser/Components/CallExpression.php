<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class CallExpression
{
  public ?Token $token = null;
  public mixed $function = null;
  public array $arguments = [];

  public static function parse(Parser $parser, $function): ?CallExpression
  {
    $expression = new CallExpression();

    $expression->token = $parser->curToken;
    $expression->function = $function;
    $expression->arguments = $expression->parseCallArguments($parser);

    return $expression;
  }

  public function parseCallArguments(Parser $parser):array
  {
    $arguments = [];

    if ($parser->peekTokenIs(TokenType::RPAREN)) {
      $parser->nextToken();

      return $arguments;
    }

    $parser->nextToken();
    $arguments[] = $parser->parseExpression(Parser::LOWEST);

    while ($parser->peekTokenIs(TokenType::COMMA)) {
      $parser->nextToken();
      $parser->nextToken();

      $arguments[] = $parser->parseExpression(Parser::LOWEST);
    }

    if (! $parser->expectPeek(TokenType::RPAREN)) {
      return [];
    }

    return $arguments;
  }
}

