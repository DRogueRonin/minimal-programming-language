<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\ExpressionStatement;
use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class InfixExpression
{
  public ?Token $token = null;
  public mixed $left = null;
  public string $operator = '';
  public mixed $right = null;

  public static function parse(Parser $parser, mixed $left): InfixExpression
  {
    $expression = new InfixExpression();

    $expression->token = $parser->curToken;
    $expression->operator = $parser->curToken->literal;
    $expression->left = $left;

    $precedence = $parser->curPrecedence();
    $parser->nextToken();
    $expression->right = $parser->parseExpression($precedence);

    return $expression;
  }
}

