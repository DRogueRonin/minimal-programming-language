<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class PrefixExpression
{
  public ?Token $token = null;
  public string $operator = '';
  public mixed $right = null;

  public static function parse(Parser $parser): PrefixExpression
  {
    $expression = new PrefixExpression();

    $expression->token = $parser->curToken;
    $expression->operator = $parser->curToken->literal;

    $parser->nextToken();

    $expression->right = $parser->parseExpression(Parser::PREFIX);

    return $expression;
  }
}

