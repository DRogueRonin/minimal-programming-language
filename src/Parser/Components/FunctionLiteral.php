<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class FunctionLiteral
{
  public ?Token $token = null;
  public array $parameters = [];
  public ?BlockStatement $body = null;

  public static function parse(Parser $parser): ?FunctionLiteral
  {
    $literal = new FunctionLiteral();

    $literal->token = $parser->curToken;

    if (! $parser->expectPeek(TokenType::LPAREN)) {
      return null;
    }

    $literal->parameters = $literal->parseParameters($parser);

    if (! $parser->expectPeek(TokenType::LBRACE)) {
      return null;
    }

    $literal->body = BlockStatement::parse($parser);

    return $literal;
  }

  public function parseParameters(Parser $parser): array
  {
    $identifiers = [];

    if ($parser->peekTokenIs(TokenType::RPAREN)) {
      $parser->nextToken();

      return $identifiers;
    }

    $parser->nextToken();

    $identifiers[] = new Identifier($parser->curToken, $parser->curToken->literal);

    while ($parser->peekTokenIs(TokenType::COMMA)) {
      $parser->nextToken();
      $parser->nextToken();

      $identifiers[] = new Identifier($parser->curToken, $parser->curToken->literal);
    }

    if (! $parser->expectPeek(TokenType::RPAREN)) {
      return [];
    }

    return $identifiers;
  }
}

