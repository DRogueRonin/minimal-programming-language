<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class IfExpression
{
  public ?Token $token = null;
  public mixed $condition = null;
  public ?BlockStatement $consequence = null;
  public mixed $alternative = null;

  public static function parse(Parser $parser): ?IfExpression
  {
    $expression = new IfExpression();

    $expression->token = $parser->curToken;

    if (! $parser->expectPeek(TokenType::LPAREN)) {
      return null;
    }

    $parser->nextToken();
    $expression->condition = $parser->parseExpression(Parser::LOWEST);

    if (! $parser->expectPeek(TokenType::RPAREN)) {
      return null;
    }

    if (! $parser->expectPeek(TokenType::LBRACE)) {
      return null;
    }

    $expression->consequence = BlockStatement::parse($parser);

    if ($parser->peekTokenIs(TokenType::ELSE)) {
      $parser->nextToken();

      if ($parser->peekTokenIs(TokenType::IF)) {
        $parser->nextToken();
        $expression->alternative = IfExpression::parse($parser);
      } else {
        if (! $parser->expectPeek(TokenType::LBRACE)) {
          return null;
        }

        $expression->alternative = BlockStatement::parse($parser);
      }
    }

    return $expression;
  }
}

