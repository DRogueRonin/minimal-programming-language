<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class Boolean
{
  public ?Token $token = null;
  public bool $value = false;

  public static function parse(Parser $parser): Boolean
  {
    $boolean = new Boolean();

    $boolean->token = $parser->curToken;
    $boolean->value = $parser->curTokenIs(TokenType::TRUE);

    return $boolean;
  }
}

