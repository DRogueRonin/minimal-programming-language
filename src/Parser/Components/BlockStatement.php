<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class BlockStatement
{
  public ?Token $token = null;
  public array $statements = [];

  public static function parse(Parser $parser): ?BlockStatement
  {
    $block = new BlockStatement();

    $block->token = $parser->curToken;

    $parser->nextToken();

    while (! $parser->curTokenIs(TokenType::RBRACE) && ! $parser->curTokenIs(TokenType::EOF)) {
      $statement = $parser->parseStatement();

      if (! is_null($statement)) {
	$block->statements[] = $statement;
      }

      $parser->nextToken();
    }

    return $block;
  }
}

