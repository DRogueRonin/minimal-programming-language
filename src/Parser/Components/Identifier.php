<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;

class Identifier
{
  public ?Token $token = null;
  public string $value = '';

  public function __construct(Token $token, string $value)
  {
    $this->token = $token;
    $this->value = $value;
  }

  public static function parse(Parser $parser): Identifier
  {
    return new Identifier($parser->curToken, $parser->curToken->literal);
  }
}

