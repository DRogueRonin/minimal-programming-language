<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class IntegerLiteral
{
  public ?Token $token = null;
  public int $value = 0;

  public static function parse(Parser $parser): ?IntegerLiteral
  {
    $literal = new IntegerLiteral();

    $literal->token = $parser->curToken;

    $value = intval($parser->curToken->literal);
    if ($value === 0 || $value === 1) {
      #$error = sprintf('could not parse %s as integer', print_r($parser->curToken->literal, true));
      #$parser->errors[] = $error;
    }

    $literal->value = $value;

    return $literal;
  }
}

