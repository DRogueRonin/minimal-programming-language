<?php

namespace Mnml\Parser\Components;

use Mnml\Parser\Parser;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;

class ReturnStatement
{
  public ?Token $token = null;
  public mixed $returnValue = null;

  public static function parse(Parser $parser): ?ReturnStatement
  {
    $statement = new ReturnStatement();

    $statement->token = $parser->curToken;

    $parser->nextToken();

    $statement->returnValue = $parser->parseExpression(Parser::LOWEST);

    if ($parser->peekTokenIs(TokenType::SEMICOLON)) {
      $parser->nextToken();
    }

    return $statement;
  }
}

