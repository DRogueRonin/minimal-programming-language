<?php

namespace Mnml\Parser;

use Mnml\Lexer\Lexer;
use Mnml\Parser\Components\Boolean;
use Mnml\Parser\Components\CallExpression;
use Mnml\Parser\Components\ExpressionStatement;
use Mnml\Parser\Components\FunctionLiteral;
use Mnml\Parser\Components\Identifier;
use Mnml\Parser\Components\IfExpression;
use Mnml\Parser\Components\InfixExpression;
use Mnml\Parser\Components\IntegerLiteral;
use Mnml\Parser\Components\PrefixExpression;
use Mnml\Lexer\Token;
use Mnml\Lexer\TokenType;
use Mnml\Parser\Components\LetStatement;
use Mnml\Parser\Components\ReturnStatement;

class Parser
{
  public Lexer $lexer;

  public ?Token $curToken = null;
  public ?Token $peekToken = null;

  public array $errors = [];

  public array $prefixParseFns = [];
  public array $infixParseFns = [];

  const LOWEST = 1;
  const LOGICAL_OR = 2;
  const LOGICAL_AND = 3;
  const EQUALS = 4; // ==
  const LESSGREATER = 5; // > or <
  const SUM = 6; // +
  const PRODUCT = 7; // *
  const PREFIX = 8; // -X or !X
  const CALL = 9; // myFunction(X)

  const PRECEDENCES = [
    TokenType::OR => Parser::LOGICAL_OR,
    TokenType::AND => Parser::LOGICAL_AND,
    TokenType::EQUAL => Parser::EQUALS,
    TokenType::NOT_EQUAL => Parser::EQUALS,
    TokenType::LOWERTHAN => Parser::LESSGREATER,
    TokenType::GREATERTHAN => Parser::LESSGREATER,
    TokenType::PLUS => Parser::SUM,
    TokenType::MINUS => Parser::SUM,
    TokenType::SLASH => Parser::PRODUCT,
    TokenType::ASTERISK => Parser::PRODUCT,
    TokenType::LPAREN => Parser::CALL,
  ];

  public function __construct(Lexer $lexer)
  {
    $this->lexer = $lexer;

    $this->nextToken();
    $this->nextToken();

    $this->registerPrefix(TokenType::IDENTIFIER, fn () => Identifier::parse($this));
    $this->registerPrefix(TokenType::INT, fn () => IntegerLiteral::parse($this));
    $this->registerPrefix(TokenType::BANG, fn () => PrefixExpression::parse($this));
    $this->registerPrefix(TokenType::MINUS, fn () => PrefixExpression::parse($this));
    $this->registerPrefix(TokenType::TRUE, fn () => Boolean::parse($this));
    $this->registerPrefix(TokenType::FALSE, fn () => Boolean::parse($this));
    $this->registerPrefix(TokenType::LPAREN, fn () => $this->parseGroupedExpression());
    $this->registerPrefix(TokenType::IF, fn () => IfExpression::parse($this));
    $this->registerPrefix(TokenType::FUNCTION, fn () => FunctionLiteral::parse($this));

    $this->registerInfix(TokenType::PLUS, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::MINUS, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::SLASH, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::ASTERISK, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::EQUAL, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::NOT_EQUAL, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::LOWERTHAN, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::GREATERTHAN, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::LPAREN, fn ($infix) => CallExpression::parse($this, $infix));
    $this->registerInfix(TokenType::AND, fn ($infix) => InfixExpression::parse($this, $infix));
    $this->registerInfix(TokenType::OR, fn ($infix) => InfixExpression::parse($this, $infix));
  }

  public function nextToken()
  {
    $this->curToken = $this->peekToken;
    $this->peekToken = $this->lexer->nextToken();
  }

  public function parseProgram()
  {
    $statements = [];
    while ($this->curToken->type !== TokenType::EOF) {
      $statements[] = $this->parseStatement();

      $this->nextToken();
    }

    return new Program($statements, $this->errors);
  }

  public function parseStatement()
  {
    switch ($this->curToken->type) {
      case TokenType::LET:
        return LetStatement::parse($this);

      case TokenType::RETURN:
        return ReturnStatement::parse($this);

      default:
        return ExpressionStatement::parse($this);
    }
  }

  public function curTokenIs(string $type): bool
  {
    return $this->curToken->type === $type;
  }

  public function peekTokenIs(string $type): bool
  {
    return $this->peekToken->type === $type;
  }

  public function expectPeek(string $type): bool
  {
    if ($this->peekTokenIs($type)) {
      $this->nextToken();

      return true;
    }

    $this->peekError($type);

    return false;
  }

  public function peekError(string $type)
  {
    $msg = sprintf('expected next token to be %s, got %s instead', $type, $this->peekToken->type);
    $this->errors[] = $msg;
  }

  public function registerPrefix(string $tokenType, callable $prefixParseFn)
  {
    $this->prefixParseFns[$tokenType] = $prefixParseFn;
  }

  public function registerInfix(string $tokenType, callable $infixParseFn)
  {
    $this->infixParseFns[$tokenType] = $infixParseFn;
  }

  public function parseExpression(int $precedence)
  {
    $prefix = $this->prefixParseFns[$this->curToken->type] ?? false;

    if (! $prefix) {
      $this->errors[] = sprintf('no prefix parse function for %s found', $this->curToken->type);

      return $this->curToken;
    }

    $leftExp = $prefix();

    while (! $this->peekTokenIs(TokenType::SEMICOLON) && $precedence < $this->peekPrecedence()) {
      $infix = $this->infixParseFns[$this->peekToken->type] ?? false;

      if (! $infix) {
        return $leftExp;
      }

      $this->nextToken();

      $leftExp = $infix($leftExp);
    }

    return $leftExp;
  }

  public function peekPrecedence(): int
  {
    return Parser::PRECEDENCES[$this->peekToken->type] ?? Parser::LOWEST;
  }

  public function curPrecedence(): int
  {
    return Parser::PRECEDENCES[$this->curToken->type] ?? Parser::LOWEST;
  }

  public function parseGroupedExpression()
  {
    $this->nextToken();

    $expression = $this->parseExpression(Parser::LOWEST);

    if (! $this->expectPeek(TokenType::RPAREN)) {
      return null;
    }

    return $expression;
  }
}

