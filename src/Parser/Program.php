<?php

namespace Mnml\Parser;

class Program
{
  public function __construct(
    public array $statements,
    public array $errors
  )
  {}
}

