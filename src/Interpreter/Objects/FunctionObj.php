<?php

namespace Mnml\Interpreter\Objects;

use Mnml\Parser\Components\BlockStatement;

class FunctionObj
{
  const TYPE = 'FUNCTION';

  public function __construct(
    public array $parameters,
    public ?BlockStatement $body,
    public Environment $env,
  ) {}

  public function get(string $name)
  {
    return $this->store[$name];
  }

  public function set(string $name, $value)
  {
    return $this->store[$name] = $value;
  }
}

