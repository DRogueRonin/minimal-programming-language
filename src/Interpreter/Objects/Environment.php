<?php

namespace Mnml\Interpreter\Objects;

class Environment
{
  public function __construct(
    public array $store = [],
    public ?Environment $outer = null,
  ) {}

  public static function newEnclosed(Environment $outer)
  {
    return new Environment([], $outer);
  }

  public function get(string $name)
  {
    $value = $this->store[$name] ?? null;

    if (! $value && $this->outer) {
      return $this->outer->get($name) ?? null;
    }

    return $value;
  }

  public function set(string $name, $value)
  {
    return $this->store[$name] = $value;
  }
}

