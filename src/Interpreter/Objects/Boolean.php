<?php

namespace Mnml\Interpreter\Objects;

class Boolean
{
  const TYPE = 'BOOLEAN';

  public function __construct(
    public bool $value,
  ) {}
}

