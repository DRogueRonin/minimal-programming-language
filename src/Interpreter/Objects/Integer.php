<?php

namespace Mnml\Interpreter\Objects;

class Integer
{
  const TYPE = 'INTEGER';

  public function __construct(
    public int $value,
  ) {}
}

