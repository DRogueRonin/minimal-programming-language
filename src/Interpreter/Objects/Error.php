<?php

namespace Mnml\Interpreter\Objects;

class Error
{
  const TYPE = 'ERROR';

  public function __construct(
    public string $message,
  ) {}
}

