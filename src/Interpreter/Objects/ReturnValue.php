<?php

namespace Mnml\Interpreter\Objects;

class ReturnValue
{
  const TYPE = 'RETURN_VALUE';

  public function __construct(
    public $value,
  ) {}
}

