<?php

namespace Mnml\Interpreter;

use Mnml\Interpreter\Objects\Boolean;
use Mnml\Interpreter\Objects\Environment;
use Mnml\Interpreter\Objects\Error;
use Mnml\Interpreter\Objects\FunctionObj;
use Mnml\Interpreter\Objects\Integer;
use Mnml\Interpreter\Objects\ReturnValue;
use Mnml\Parser\Components\BlockStatement;
use Mnml\Parser\Components\Boolean as ParsedBoolean;
use Mnml\Parser\Components\CallExpression;
use Mnml\Parser\Components\ExpressionStatement;
use Mnml\Parser\Components\FunctionLiteral;
use Mnml\Parser\Components\Identifier;
use Mnml\Parser\Components\IfExpression;
use Mnml\Parser\Components\InfixExpression;
use Mnml\Parser\Components\IntegerLiteral;
use Mnml\Parser\Components\LetStatement;
use Mnml\Parser\Components\PrefixExpression;
use Mnml\Parser\Components\ReturnStatement;
use Mnml\Parser\Expression;
use Mnml\Parser\Program;

class Interpreter
{
  public static ?Boolean $TRUE = null;
  public static ?Boolean $FALSE = null;

  public function __construct()
  {
    self::$TRUE = new Boolean(true);
    self::$FALSE = new Boolean(false);
  }


  public function eval($node, Environment $env)
  {
    // Program
    if ($node instanceof Program) {
      return $this->evalProgram($node, $env);
    }

    // BlockStatement
    if ($node instanceof BlockStatement) {
      return $this->evalBlockStatement($node, $env);
    }

    // ExpressionStatement
    else if ($node instanceof ExpressionStatement) {
      return $this->eval($node->expression, $env);
    }

    // LetSatement
    else if ($node instanceof LetStatement) {
      $value = $this->eval($node->value, $env);

      if ($value instanceof Error) {
        return $value;
      }

      $env->set($node->name->value, $value);

      return $value;
    }

    // ReturnStatement
    else if ($node instanceof ReturnStatement) {
      $value = $this->eval($node->returnValue, $env);

      if ($value instanceof Error) {
        return $value;
      }

      return new ReturnValue($value);
    }

    // InfixExpression
    else if ($node instanceof InfixExpression) {
      $left = $this->eval($node->left, $env);
      if ($left instanceof Error) {
        return $left;
      }

      $right = $this->eval($node->right, $env);
      if ($right instanceof Error) {
        return $right;
      }

      return $this->evalInfixExpression($node->operator, $left, $right, $env);
    }

    // PrefixExpression
    else if ($node instanceof PrefixExpression) {
      $right = $this->eval($node->right, $env);

      if ($right instanceof Error) {
        return $right;
      }

      return $this->evalPrefixExpression($node->operator, $right);
    }

    // IfExpression
    else if ($node instanceof IfExpression) {
      $condition = $this->eval($node->condition, $env);

      if ($condition instanceof Error) {
        return $condition;
      }

      return $this->evalIfExpression($node, $env);
    }

    // CallExpression
    else if ($node instanceof CallExpression) {
      $function = $this->eval($node->function, $env);

      if ($function instanceof Error) {
        return $function;
      }

      $arguments = $this->evalExpressions($node->arguments, $env);
      if (count($arguments) === 1 && $arguments[0] instanceof Error) {
        return $arguments[0];
      }

      return $this->applyFunction($function, $arguments);
    }

    // Identifier
    else if ($node instanceof Identifier) {
      return $this->evalIdentifier($node, $env);
    }

    // Integer
    else if ($node instanceof IntegerLiteral) {
      return new Integer($node->value);
    }

    // Boolean
    else if ($node instanceof ParsedBoolean) {
      return $this->nativeBoolToBooleanObject($node->value);
    }

    // Function
    else if ($node instanceof FunctionLiteral) {
      return new FunctionObj($node->parameters, $node->body, $env);
    }

    return $node;
  }

  public function evalProgram($program, Environment $env)
  {
    $result = null;

    foreach ($program->statements as $statement) {
      $result = $this->eval($statement, $env);

      if ($result instanceof ReturnValue) {
        return $result->value;
      }
      else if ($result instanceof Error) {
        return $result;
      }
    }

    return $result;
  }

  public function evalBlockStatement($block, Environment $env)
  {
    $result = null;

    foreach ($block->statements as $statement) {
      $result = $this->eval($statement, $env);

      if ($result instanceof ReturnValue || $result instanceof Error) {
        return $result;
      }
    }

    return $result;
  }

  public function nativeBoolToBooleanObject($bool) {
    return $bool ? Interpreter::$TRUE : Interpreter::$FALSE;
  }

  public function evalPrefixExpression($operator, $right)
  {
    if ($operator === '!') {
      return $this->evalBangOperatorExpression($right);
    }
    else if ($operator === '-') {
      return $this->evalMinusPrefixOperatorExpression($right);
    }

    return new Error(sprintf('unknown operator: %s%s', $operator, get_class($right)));
  }

  public function evalBangOperatorExpression($right)
  {
    if (! ($right instanceof Boolean)) {
      return new Error(sprintf('unknown operator: !%s', get_class($right)));
    }

    if ($right->value === true) {
      return Interpreter::$FALSE;
    }
    else if ($right->value === false) {
      return Interpreter::$TRUE;
    }

    return null;
  }

  public function evalMinusPrefixOperatorExpression($right)
  {
    if (! ($right instanceof Integer)) {
      return new Error(sprintf('unknown operator: -%s', get_class($right)));
    }

    return new Integer(-$right->value);
  }

  public function evalInfixExpression($operator, $left, $right, Environment $env)
  {
    if ($left instanceof Integer && $right instanceof Integer) {
      return $this->evalIntegerInfixExpression($operator, $left, $right);
    }

    else if ($operator == '==') {
      return $this->nativeBoolToBooleanObject($left == $right);
    }
    else if ($operator == '!=') {
      return $this->nativeBoolToBooleanObject($left != $right);
    }
    else if ($operator == '&&') {
      return $this->nativeBoolToBooleanObject($left->value && $right->value);
    }
    else if ($operator == '||') {
      return $this->nativeBoolToBooleanObject($left->value || $right->value);
    }

    else if (get_class($left) !== get_class($right)) {
      return new Error(sprintf('type mismatch: %s %s %s', get_class($left), $operator, get_class($right)));
    }

    return new Error(sprintf('unknown operator: %s %s %s', get_class($left), $operator, get_class($right)));
  }

  public function evalIntegerInfixExpression($operator, $left, $right)
  {
    $leftValue = $left->value;
    $rightValue = $right->value;

    switch ($operator) {
      case '+':
        return new Integer($leftValue + $rightValue);

      case '-':
        return new Integer($leftValue - $rightValue);

      case '*':
        return new Integer($leftValue * $rightValue);

      case '/':
        return new Integer($leftValue / $rightValue);

      case '<':
        return $this->nativeBoolToBooleanObject($leftValue < $rightValue);

      case '>':
        return $this->nativeBoolToBooleanObject($leftValue > $rightValue);

      case '==':
        return $this->nativeBoolToBooleanObject($leftValue == $rightValue);

      case '!=':
        return $this->nativeBoolToBooleanObject($leftValue != $rightValue);

      default:
        return new Error(sprintf('unknown operator: %s %s %s', get_class($left), $operator, get_class($right)));
    }
  }

  public function evalIfExpression(IfExpression $node, Environment $env)
  {
    $condition = $this->eval($node->condition, $env);

    if ($condition === Interpreter::$TRUE) {
      return $this->eval($node->consequence, $env);
    }
    else if (! is_null($node->alternative)) {
      return $this->eval($node->alternative, $env);
    }

    return null;
  }

  public function evalIdentifier($node, Environment $env)
  {
    $value = $env->get($node->value);

    if (! $value) {
      return new Error('identifier not found: ' . $node->value);
    }

    return $value;
  }

  public function evalExpressions($expressions, $env)
  {
    $result = null;

    foreach ($expressions as $expression) {
      $evaluated = $this->eval($expression, $env);

      if ($evaluated instanceof Error) {
        return [$evaluated];
      }

      $result[] = $evaluated;
    }

    return $result;
  }

  public function applyFunction($function, $arguments) {
    if (! ($function instanceof FunctionObj)) {
      return new Error(sprintf('not a functions: %s', get_class($function)));
    }

    $extendedEnv = $this->extendFunctionEnv($function, $arguments);
    $evaluated = $this->eval($function->body, $extendedEnv);

    return $this->unwrapReturnValue($evaluated);
  }

  public function extendFunctionEnv($function, $arguments)
  {
    $env = Environment::newEnclosed($function->env);

    foreach ($function->parameters as $idx => $parameter) {
      $env->set($parameter->value, $arguments[$idx]);
    }

    return $env;
  }

  public function unwrapReturnValue($obj)
  {
    if ($obj instanceof ReturnValue) {
      return $obj->value;
    }

    return $obj;
  }
}

