<?php

namespace Mnml\Lexer;

class Token
{
  public string $type;
  public string $literal;

  /**
   * Token constructor.
  */
  public function __construct(string $type, string $literal)
  {
    $this->type = $type;
    $this->literal = $literal;
  }
}

