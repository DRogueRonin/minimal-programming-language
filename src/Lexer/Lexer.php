<?php

namespace Mnml\Lexer;

class Lexer
{
  private string $input;
  private int $position = 0;
  private int $readPosition = 0;
  private string|int $ch = 0;

  /**
   * Lexer constructor.
   */
  public function __construct(string $input)
  {
    $this->input = $input;

    $this->readChar();
  }

  /**
   * Read next character
   */
  public function readChar()
  {
    if ($this->readPosition >= strlen($this->input)) {
      $this->ch = 0;
    } else {
      $this->ch = $this->input[$this->readPosition];
    }

    $this->position = $this->readPosition;
    $this->readPosition += 1;
  }

  /**
   * Identify next Token
   */
  public function nextToken(): Token
  {
    $this->skipWhitespace();

    $tokenTypes = TokenType::getTypes();
    $tokenLiterals = array_flip($tokenTypes);

    $typeConstant = $tokenLiterals[$this->ch] ?? false;
    $type = $tokenTypes[$typeConstant] ?? false;
    $literal = $this->ch;

    // EOF
    if ($this->ch === 0) {
      $token = new Token(TokenType::EOF, '');
    }

    // TokenTypes that match the literal
    else if ($type !== false) {

      // =
      if ($type === TokenType::ASSIGN) {

        // ==
        if ($this->peekChar() === '=') {
          $ch = $this->ch;
          $this->readChar();

          $literal = $ch . $this->ch;
          $type = TokenType::EQUAL;
        }
      }

      // !
      else if ($type === TokenType::BANG) {

        // !=
        if ($this->peekChar() === '=') {
          $ch = $this->ch;
          $this->readChar();

          $literal = $ch . $this->ch;
          $type = TokenType::NOT_EQUAL;
        }
      }

      // &
      else if ($type === TokenType::BIT_AND) {

        // &&
        if ($this->peekChar() === '&') {
          $ch = $this->ch;
          $this->readChar();

          $literal = $ch . $this->ch;
          $type = TokenType::AND;
        }
      }

      // |
      else if ($type === TokenType::BIT_OR) {

        // ||
        if ($this->peekChar() === '|') {
          $ch = $this->ch;
          $this->readChar();

          $literal = $ch . $this->ch;
          $type = TokenType::OR;
        }
      }

      $token = new Token($type, $literal);
    }

    // Identifiers
    else if ($this->isLetter($this->ch)) {
      $literal = $this->readIdentifier();
      $type = TokenType::lookupIdentifier($literal);

      return new Token($type, $literal);
    }

    // Numbers
    else if ($this->isDigit($this->ch)) {
      $literal = $this->readNumber();
      $type = TokenType::INT;

      return new Token($type, $literal);
    }

    // unrecognized tokens
    else {
      $token = new Token(TokenType::ILLEGAL, $this->ch);
    }

    $this->readChar();

    return $token;
  }

  /**
   * Identify identifiers
   *
   * @return string
   */
  public function readIdentifier(): string
  {
    $position = $this->position;

    while ($this->isLetter($this->ch)) {
      $this->readChar();
    }

    return substr($this->input, $position, $this->position - $position);
  }

  /**
   * Check whether char is a letter
   *
   * @param string $ch
   *
   * @return bool
   */
  public function isLetter(string $ch): bool
  {
    return 'a' <= $ch && $ch <= 'z' || 'A' <= $ch && $ch <= 'Z' || $ch === '_';
  }

  /**
   * Ignore whitespace characters
   */
  public function skipWhitespace()
  {
    $haystack = [
      '
', // newline
      ' ',
      "\t",
      "\n",
      "\r",
    ];

    while (in_array($this->ch, $haystack)) {
      $this->readChar();
    }
  }

  /**
   * Identify numbers
   *
   * @return string
   */
  public function readNumber(): string
  {
    $position = $this->position;

    while ($this->isDigit($this->ch)) {
      $this->readChar();
    }

    return substr($this->input, $position, $this->position - $position);
  }

  /**
   * Check whether char is a digit
   *
   * @param string $ch
   *
   * @return bool
   */
  public function isDigit(string $ch): bool
  {
    return '0' <= $ch && $ch <= '9';
  }

  /**
   * Peek ahead
   *
   * @return string
   */
  public function peekChar(): string
  {
    if ($this->readPosition >= strlen($this->input)) {
      return 0;
    } else {
      return $this->input[$this->readPosition];
    }
  }
}

