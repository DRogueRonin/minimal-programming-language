<?php

namespace Mnml\Lexer;

class TokenType
{
  const ILLEGAL = 'ILLEGAL';
  const EOF = 'EOF';

  const IDENTIFIER = 'IDENTIFIER';
  const INT = 'INT';

  const ASSIGN = '=';
  const PLUS = '+';
  const MINUS ='-';
  const BANG = '!';
  const ASTERISK = '*';
  const SLASH = '/';

  const LOWERTHAN = '<';
  const GREATERTHAN = '>';

  const COMMA = ',';
  const SEMICOLON = ';';

  const LPAREN = '(';
  const RPAREN = ')';
  const LBRACE = '{';
  const RBRACE = '}';
  const LBRACKET = '[';
  const RBRACKET = ']';

  const FUNCTION = 'FUNCTION';
  const CONST = 'CONST';
  const LET = 'LET';
  const TRUE = 'TRUE';
  const FALSE = 'FALSE';
  const IF = 'IF';
  const ELSE = 'ELSE';
  const RETURN = 'RETURN';

  const EQUAL = '==';
  const NOT_EQUAL = '!=';

  const BIT_AND = '&';
  const BIT_OR = '|';
  const AND = '&&';
  const OR = '||';

  /**
   * Get all defined types
   *
   * @return array
   */
  public static function getTypes()
  {
    $ref = new \ReflectionClass(__CLASS__);

    return $ref->getConstants();
  }

  /**
   * Get type of Identifier
   *
   * @param string $literal
   *
   * @return string
   */
  public static function lookupIdentifier(string $literal): string
  {
    $keywords = [
      'function' => TokenType::FUNCTION,
      'const' => TokenType::CONST,
      'let' => TokenType::LET,
      'true' => TokenType::TRUE,
      'false' => TokenType::FALSE,
      'if' => TokenType::IF,
      'else' => TokenType::ELSE,
      'return' => TokenType::RETURN,
    ];

    return $keywords[$literal] ?? TokenType::IDENTIFIER;
  }
}

